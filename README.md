# Interest Rates Calculator

## Description
This project is a simple implementation of a savings account interest calculator.



## Installation

Use the package manager [npm](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) to add dependencies:

```bash
npm install
```

## Usage

Run the following command to execute the interest calculator:

```bash
npm run calculate [balance]
```

Example:
```bash
npm run calculate 1000
```

## Testing

Run the following command to execute the tests:

```bash
npm test
```

## Author

- Name: Renat Khusainov
- Email: renat.khusainov@gmail.com

