import { calculateInterest } from './calculateInterest';

describe('calculateInterest', () => {


    test('checks interest for first tier and round floor', () => {
        expect(calculateInterest(224.4)).toBe(2.24);
    });

    test('checks the separation of the first and second tiers', () => {
        expect(calculateInterest(1000)).toBe(15.00);
    })

    test('checks interest for second tier and round ceil', () => {
        expect(calculateInterest(3001)).toBe(45.02);
    });

    test('checks the separation of the second and third tiers', () => {
        expect(calculateInterest(5000)).toBe(100.00);
    });

    test('checks interest for third tier', () => {
        expect(calculateInterest(10001)).toBe(250.03);
    });

    test('checks the separation of the third and fourth tiers', () => {
        expect(calculateInterest(50000)).toBe(1500.00);
    });

    test('checks interest for the last tier', () => {
        expect(calculateInterest(50003)).toBe(1500.09);
    })

    test('checks interest for zero amount', () => {
        expect(calculateInterest(0)).toBe(0);
    });

    test('checks interest for negative amount', () => {
        expect(calculateInterest(-100000)).toBe(0);
    });

    test('checks interest for big amount and there is no more tiers then the last', () => {
        expect(calculateInterest(900719925474099)).toBe(27021597764222.97);
    });

});