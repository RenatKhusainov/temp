type InterestTier = {
    start: number;
    end: number;
    rate: number;
};

const interestTiers: InterestTier[] = [
    { start: 0, end: 1000, rate: 1 },
    { start: 1000, end: 5000, rate: 1.5 },
    { start: 5000, end: 10000, rate: 2.0 },
    { start: 10000, end: 50000, rate: 2.5 },
    { start: 50000, end: Infinity, rate: 3.0 },
];

/**
 * Calculates the interest based on the given balance.
 *
 * @param {number} balance - The balance to calculate the interest for.
 * @return {number} The calculated interest.
 *
 * TODO the maximum safe integer you can work with in JavaScript is 2**53 - 1
 *  need to discuss it with BA, is it possible to have savings account more 900719925474099 ?
 *  Other-way to use the type bigint
 */
function calculateInterest(balance: number): number {
    const tier = interestTiers.find(
        tier => balance >= tier.start && balance < tier.end
    );
    const rate = tier ? tier.rate : 0;
    balance = balance>0?balance:0;
    return Math.round(balance * rate)/100;
}

/**
 * Using the method in the console to print the result.
 */
const args = process.argv.slice(2);
if (args.length !== 1) {
    console.error('Please provide one argument for balance.');
    process.exit(1);
}
const balance = parseFloat(args[0]);
if (isNaN(balance)) {
    console.error('Please provide a valid number for balance.');
    process.exit(1);
}

const interest = calculateInterest(balance);
console.log(`The interest is: ${interest}. Have a good day!`);

export { calculateInterest };